<?php

namespace App\Observers;

use App\Campaign;
use App\Events\ProductUpdateEvent;
use App\Http\Controllers\Api\v1\ProductsController;
use App\Product;
use Morilog\Jalali\Jalalian;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        //
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        $products = $this->getProducts();
        event(new ProductUpdateEvent($products));
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
    public function getProducts()
    {
        $campaign = Campaign::latest()->firstOrFail();
        $productIds = '';

        $currentTimeStamp = Jalalian::forge(now())->getTimestamp();
        $campaignStartTimeStamp = Jalalian::forge($campaign->start)->getTimestamp();
        $campaignEndTimeStamp = Jalalian::forge($campaign->end)->getTimestamp();

        if ($currentTimeStamp >= $campaignStartTimeStamp && $currentTimeStamp <= $campaignEndTimeStamp) {
            if ($campaign->products) {
                $productIds = \Opis\Closure\unserialize($campaign->products);
            }
        } else {
            return response()->json('no campaign', 404);
        }
        return product::where('status', 1)->whereIn('id', $productIds)->with(['category', 'city'])->orderBy('created_at', 'desc')->get();
    }
}
