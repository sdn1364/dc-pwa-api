<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $guarded = [];


    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }
    public function city()
    {
        return $this->belongsTo(\App\City::class);
    }
    public function activate(){
        $this->status = !$this->status;
        $this->update();
    }
}
