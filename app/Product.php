<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];


    public function vendor()
    {
        return $this->belongsTo(\App\Vendor::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    public function city()
    {
        return $this->belongsTo(\App\City::class);
    }

    public function activate(){
        $this->status = !$this->status;
        $this->update();
    }
    public function feature(){
        $this->is_featured = !$this->is_featured;
        $this->update();
    }
    public function dec(){
        if($this->quantity > 0){
            $this->quantity -= 1;
        }else{
            if($this->max_reserved > 0){
                $this->max_reserved -= 1;
            }else{
                return null;
            }
        }
        $this->update();
    }
    public function inc(){
        if($this->quantity > 0){
            $this->quantity += 1;
        }else{
            if($this->max_reserved > 0){
                $this->max_reserved =+ 1;
            }else{
                return null;
            }
        }
        $this->update();
    }
}
