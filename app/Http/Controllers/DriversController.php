<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Imports\DriversImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DriversController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        $driverCount = Driver::count();
        return view('pages.driver.create',compact('driverCount'));
    }


    public function store()
    {
        /*        request()->validate([
                    'file' => 'required|mimes:csv,txt'
                ]);*/
        if (request('file')) {
            $file = request('file');
            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();
            $location = 'uploads';
            $file->move($location, $filename);
            $filepath = public_path($location . "/" . $filename);

            Excel::import(new DriversImport, $filepath);
            return redirect()->route('driver.create');
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy()
    {

    }

    public function clear()
    {
        Driver::query()->truncate();
        return redirect()->route('driver.create');
    }
}
