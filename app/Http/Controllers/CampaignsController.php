<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Morilog\Jalali\Jalalian;
use \Morilog\Jalali\CalendarUtils as CalendarUtils;
class CampaignsController extends Controller
{

    public function index()
    {
        $campaigns = Campaign::paginate('15');
        return view('pages.campaigns.list',compact('campaigns'));
    }

    public function create()
    {
        $products = Product::where('status',1)->get();
        return view('pages.campaigns.create',compact('products'));
    }

    public function store()
    {
        $startDateTimeStr = request('startDate').' ' .request('startTime').':00';
        $endDateTimeStr = request('endDate').' ' .request('endTime').':00';
        $campaign = new Campaign();
        $campaign->name = request('name');
        $campaign->start($startDateTimeStr);
        $campaign->end($endDateTimeStr);
        $campaign->products = serialize(request('selectedProducts'));
        $campaign->save();

        return redirect()->route('campaign.index');

    }

    public function show(Campaign $campaign)
    {
        //
    }


    public function edit(Campaign $campaign)
    {
        //
    }


    public function update(Request $request, Campaign $campaign)
    {
        //
    }

    public function destroy(Campaign $campaign)
    {
        $campaign->delete();
        return redirect()->route('campaign.index');
    }
}
