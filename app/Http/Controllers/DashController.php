<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashController extends Controller
{

    public function toSells(){
        return redirect('sellsDashboard');
    }

    public function sell()
    {
        return view('dashboards.home');
    }

    public function ticket()
    {
        return view('dashboards.ticket');
    }

    public function stats()
    {
        return view('dashboards.stats');
    }
}
