<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Product;
use App\Vendor;
use Intervention\Image\ImageManagerStatic as Image;
class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::orderBy('created_at','desc')->paginate(15);

        return view('pages.products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all();
        $vendors = Vendor::all();
        $cities = City::all();
        return view('pages.products.create',compact(['categories','cities','vendors']));
    }

    public function store()
    {


        if(request('submit')){
            $product = Product::create(request()->all());
            $this->storeImage($product);
            return redirect()->route('product.index')->with('success','محصول مورد نظر ایجاد شد.');
        }
        if(request('submitNew')){
            $product = Product::create(request()->all());
            $this->storeImage($product);
            request()->session()->flash('product.title', $product->title);
            return redirect()->route('product.create')->with('success','محصول مورد نظر ایجاد شد.');
        }


    }


    public function edit(Product $product)
    {
        $categories = Category::all();
        $vendors = Vendor::all();
        $cities = City::all();
        return view('pages.products.edit', compact(['product','categories','cities','vendors']));
    }

    public function update(Product $product)
    {
        $product = Product::find($product);
        $this->storeImage($product);
        return redirect()->route('product.index');
    }

    public function destroy( Product $product)
    {
        $product->delete();
        return redirect()->route('product.index');
    }

    private function storeImage($product)
    {
        if (request()->has('photo')) {
            $file = request()->file('photo');
            $fileName = $file->getClientOriginalName();
            $generatedFileName = uniqid().'-'.$fileName;
            Image::make(request()->photo)->save(public_path('uploads/images/'.$generatedFileName),100,'jpg');
            $product->update([
                'photo' => $generatedFileName
            ]);
        }
    }
}
