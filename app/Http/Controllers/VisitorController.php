<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller
{

    public function index(Request $request)
    {
        $visitors = Visitor::all();

        return view('pages.visitors.index', compact('visitors'));
    }

    public function store(Request $request)
    {
        $visitor = Visitor::create($request->all());
    }
}
