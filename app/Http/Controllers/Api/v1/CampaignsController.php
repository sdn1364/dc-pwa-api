<?php

namespace App\Http\Controllers\Api\v1;

use App\Campaign;
use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;

class CampaignsController extends Controller
{
    public function getLatestCampaign()
    {
        $campaign = Campaign::latest()->firstOrFail();
        $currentTimeStamp = Jalalian::forge(now())->getTimestamp();
        $campaignStartTimeStamp = Jalalian::forge($campaign->start)->getTimestamp();
        $campaignEndTimeStamp = Jalalian::forge($campaign->end)->getTimestamp();
        if ($currentTimeStamp >= $campaignStartTimeStamp && $currentTimeStamp <= $campaignEndTimeStamp) {
            return response()->json(['type' => 'inCampaign']);
        } else {
            if ($currentTimeStamp <= $campaignStartTimeStamp) {
                return response()->json(['date' => $campaign->start, 'type' => 'waiting']);
            } else {
                return response()->json('no campaign', 404);
            }
        }
    }
}
