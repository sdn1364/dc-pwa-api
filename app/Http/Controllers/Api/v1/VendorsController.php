<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Vendor;
use Illuminate\Http\Request;

class VendorsController extends Controller
{
    public function index(){
        $vendors = Vendor::all();
        return response()->json($vendors);
    }

    public function activate(Vendor $vendor)
    {
        $vendor->activate();
    }
}
