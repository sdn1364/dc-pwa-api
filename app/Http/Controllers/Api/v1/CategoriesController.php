<?php

namespace App\Http\Controllers\api\v1;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\v1\CategoryCollection;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(){
        $category = Category::all();
        return response()->json($category);
    }

    public function activate(Category $category)
    {
        $category->activate();
    }
}
