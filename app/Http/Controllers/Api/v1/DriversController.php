<?php

namespace App\Http\Controllers\Api\v1;

use App\Driver;
use App\Http\Controllers\Controller;
use App\Utilities;
use Illuminate\Http\Request;

class DriversController extends Controller
{
    public function checkDriver()
    {

        $this->validate(request(), [
            'number' => 'required',
        ]);

        // let's find the referred driver at first
        $driver = Driver::where('phone', '=',Utilities::standardizePhone(request('number')))->first();

        // does it exist?
        if ($driver == null) {
            return response()->json('driver not found');
        }

        return response()->json('driver found', 204);
    }
}
