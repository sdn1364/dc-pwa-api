<?php

namespace App\Http\Controllers\Api\v1;

use App\Campaign;
use App\Events\ProductUpdatedEvent;
use App\Events\ProductUpdateEvent;
use App\Http\Controllers\Controller;
use App\Product;
use http\Env\Response;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return response()->json($products);
    }

    public function activate(Product $product)
    {
        $product->activate();
    }

    public function feature(Product $product)
    {
        $product->feature();
    }

    public function getFeatured()
    {
        $featured_product = Product::where('is_featured', true)->get();
        return response()->json($featured_product);
    }

    public function getLatest()
    {
        $latest_products = $this->getProducts();
        return response()->json($latest_products);

    }

    public function getOne(Product $product)
    {
        $product->load('city');
        return response()->json($product);
    }

    public function decrementingProduct(Product $product)
    {
        $product->dec();
       // $products = $this->getProducts();
        //
       // return response()->json($product);
    }

    public function incrementingProduct(Product $product)
    {
        $product->inc();
    }

    public function getProducts()
    {
        $campaign = Campaign::latest()->firstOrFail();
        $productIds = '';

        $currentTimeStamp = Jalalian::forge(now())->getTimestamp();
        $campaignStartTimeStamp = Jalalian::forge($campaign->start)->getTimestamp();
        $campaignEndTimeStamp = Jalalian::forge($campaign->end)->getTimestamp();

        if ($currentTimeStamp >= $campaignStartTimeStamp && $currentTimeStamp <= $campaignEndTimeStamp) {
            if ($campaign->products) {
                $productIds = \Opis\Closure\unserialize($campaign->products);
            }
        } else {
            return response()->json('no campaign', 404);
        }
        $latest_products = product::where('status', 1)->whereIn('id', $productIds)->with(['category', 'city'])->orderBy('created_at', 'desc')->get();
        return $latest_products;
    }
}
