<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Purchased;
use Illuminate\Http\Request;

class PurchasesController extends Controller
{
    public function add()
    {
        $purchased = new Purchased();
        $purchased->number = request('number');
        $purchased->product = request('product');
        return response()->json();

    }
}
