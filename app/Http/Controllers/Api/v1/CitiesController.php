<?php

namespace App\Http\Controllers\Api\v1;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function all()
    {
        $cities =  City::all();
        return response()->json($cities);
    }
    public function activate(City $category)
    {
        $category->activate();
    }
}
