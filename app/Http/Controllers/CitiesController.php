<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiesController extends Controller
{

    public function index()
    {
        $cities = City::paginate(15);
        return view('pages.city.index',compact('cities'));
    }


    public function create()
    {
        return view('pages.city.create');
    }


    public function store()
    {
        if(request('submit')){
            $city = City::create($this->validateData());
            return redirect(route('city.index'))->with('success','شهر مورد نظر ایجاد شد');

        }
        if(request('submitNew')){
            $city = City::create($this->validateData());
            return redirect(route('city.create'))->with('success','شهر مورد نظر ایجاد شد');

        }

    }

    public function show($id)
    {
        //
    }

    public function edit(City $city)
    {
        return view('pages.city.edit', compact('city'));
    }

    public function update(City $city)
    {
        $city->update($this->validateData());

        return redirect(route('city.index'));
    }

    public function destroy(City $city)
    {
        $city->delete();
        return redirect(route('city.index'));
    }

    private function validateData()
    {
        return request()->validate([
            'city'=> 'required',
        ]);
    }
}
