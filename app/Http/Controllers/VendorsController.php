<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Vendor;
use Illuminate\Http\Request;

class VendorsController extends Controller
{

    public function index()
    {
        $vendors = Vendor::paginate(15);

        return view('pages.vendors.list', compact('vendors'));
    }

    public function create(Request $request)
    {
        $categories  = Category::all();
        $cities  = City::all();
        return view('pages.vendors.create', compact(['categories','cities']));
    }


    public function store()
    {
        if(request('submit')){
            $vendor = Vendor::create(request()->all());

            request()->session()->flash('vendor.name', $vendor->name);

            return redirect()->route('supplier.index')->with('success','تامین‌کننده مورد نظر ایجاد شد');
        }
        if(request('submitNew')){
            $vendor = Vendor::create(request()->all());

            request()->session()->flash('vendor.name', $vendor->name);

            return redirect()->route('supplier.create')->with('success','تامین‌کننده مورد نظر ایجاد شد');
        }

    }

    public function edit(Request $request, Vendor $vendor)
    {
        $vendor = Vendor::find($vendor);

        return view('pages.vendors.edit', compact('vendor'));
    }

    public function update(Vendor $vendor)
    {
        request()->session()->flash('vendor.name', $vendor->name);
        return redirect()->route('supplier.index');
    }

    public function destroy(Request $request, Vendor $vendor)
    {
        $vendor->delete();

        return redirect()->route('supplier.index');
    }
}
