<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function index(){
        $notifs = Notification::paginate(15);
        return view('pages.notifications.list',compact('notifs'));
    }
}
