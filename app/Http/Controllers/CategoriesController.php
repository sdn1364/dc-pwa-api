<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Category::with('parent')->orderBy('created_at','desc')->paginate(15);
        return view('pages.category.list',compact('categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('pages.category.create',compact('categories'));
    }

    public function store()
    {
        if(request('submit')){
            $category = Category::create($this->validateData());
            $this->storeImage($category);

            return redirect(route('category.index'))->with('success','دسته‌بندی مورد نظر ایجاد شد');
        }
        if(request('submitNew')){
            $category = Category::create($this->validateData());
            $this->storeImage($category);
            return redirect(route('category.create'))->with('success','دسته‌بندی مورد نظر ایجاد شد');
        }

    }

    public function show(Category $category)
    {
        return view('pages.category.show',compact('category'));
    }

    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('pages.category.edit',compact(['category','categories']));
    }
    public function update(Category $category)
    {
        $category->update($this->validateData());
        $this->storeImage($category);
        return redirect(route('category.index'))->with('success','دسته‌بندی مورد نظر ویرایش شد');;
    }

    public function destroy(Category $category)
    {

        $category->delete();
        return redirect(route('category.index'));
    }

    private function validateData()
    {
        return request()->validate([
            'name'=> 'required',
            'parent_id'=>'sometimes',
            'status'=> 'sometimes',
            'photo'=>'sometimes|file|image|max:1000'
        ]);
    }

    private function storeImage($category)
    {
        if (request()->has('photo')) {
            $category->update([
                'photo' => request()->photo->store('uploads', 'public'),
            ]);
        }
    }

    public function search()
    {
        $categories = Category::where('parent_id', 0)->get();
        return view('pages.category.search',compact('categories'));
    }
}
