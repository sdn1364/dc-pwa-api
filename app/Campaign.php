<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Morilog\Jalali\CalendarUtils;

class Campaign extends Model
{
    protected $guarded=[];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function start($value){
        $this->attributes['start'] = CalendarUtils::createDateTimeFromFormat('Y/m/d H:i:s',$value);
    }
    public function end($value){
        $this->attributes['end'] = CalendarUtils::createDateTimeFromFormat('Y/m/d H:i:s',$value);
    }
}
