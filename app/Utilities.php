<?php
namespace App;

use Illuminate\Support\Str;

class Utilities
{
    /**
     * @param $phoneNumber
     * @return string
     */
    public static function standardizePhone($phoneNumber)
    {
        if (Str::startsWith($phoneNumber, '+98')) {
            return $phoneNumber;
        }
        if (Str::startsWith($phoneNumber, '0098')) {
            $phoneNumber = substr($phoneNumber, 4);
        } elseif (Str::startsWith($phoneNumber, '09')) {
            $phoneNumber = substr($phoneNumber, 1);
        }

        return '98' . $phoneNumber;
    }
}
