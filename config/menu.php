<?php

return [
    'dashboards' => [
        'name' => 'dashboards',
        'title' => 'ویکلی کمپین',
        'icon' => 'fad fa-chart-pie',
        'link' => 'sellsDashboard ticketDashboard statsDashboard',
        'body' => [
            [
                'type' => 'title',
            ],
            [
                'type' => 'link',
                'name' => 'فروش و مدیریت مشتری',
                'link' => 'sellsDashboard',
                'title' => 'sellsDashboard'
            ],
            [
                'type' => 'link',
                'name' => 'آمار وبسایت',
                'link' => '/statsDashboard',
                'title' => 'statsDashboard'
            ],
        ]
    ],
    'references' => [
        'name' => 'references',
        'title' => 'جداول مرجع',
        'icon' => 'fad fa-clone',
        'link' => 'category supplier city',
        'body' => [
            [
                'type' => 'title',
            ],
            [
                'type' => 'sub',
                'title' => 'category',
                'name' => 'دسته‌بندی‌ها',
                'submenu' => [
                    [
                        'type' => 'link',
                        'title' => 'category',
                        'name' => 'لیست دسته‌بندی‌ها',
                        'link' => 'category.index'
                    ],
                    [
                        'type' => 'link',
                        'title' => '/category/create',
                        'name' => 'دسته‌بندی جدید',
                        'link' => 'category.create'
                    ],
                ]
            ],
            [
                'type' => 'sub',
                'title' => 'city',
                'name' => 'شهرها',
                'submenu' => [
                    [
                        'type' => 'link',
                        'title' => 'city',
                        'name' => 'لیست شهرها',
                        'link' => 'city.index'
                    ],
                    [
                        'type' => 'link',
                        'title' => 'city/create',
                        'name' => 'شهر جدید',
                        'link' => 'city.create'
                    ],
                ]
            ],
            [
                'type' => 'sub',
                'title' => 'supplier',
                'name' => 'تامین‌کننده‌ها',
                'submenu' => [
                    [
                        'type' => 'link',
                        'title' => '/supplier',
                        'name' => 'لیست تامین‌کننده‌ها',
                        'link' => 'supplier.index'
                    ],
                    [
                        'type' => 'link',
                        'title' => 'supplier/create',
                        'name' => 'تامین‌کننده جدید',
                        'link' => 'supplier.create'
                    ],
                ]
            ],
        ]
    ],
    'product' => [
        'name' => 'product',
        'title' => 'محصولات',
        'icon' => 'fad fa-box',
        'link' => 'campaigns product',
        'body' => [
            [
                'type' => 'title',
            ],
            [
                'type' => 'sub',
                'title' => 'product',
                'name' => 'محصولات',
                'submenu' => [
                    [
                        'type' => 'link',
                        'title' => 'product',
                        'name' => 'لیست محصولات',
                        'link' => 'product.index'
                    ],
                    [
                        'type' => 'link',
                        'title' => 'product/create',
                        'name' => 'محصول جدید',
                        'link' => 'product.create'
                    ],
                ]
            ],
            [
                'type' => 'sub',
                'title' => 'campaign',
                'name' => 'کمپین‌ها',
                'submenu' => [
                    [
                        'type' => 'link',
                        'title' => 'campaign',
                        'name' => 'لیست کمپین‌ها',
                        'link' => 'campaign.index'
                    ],
                    [
                        'type' => 'link',
                        'title' => 'campaign/create',
                        'name' => 'کمپین جدید',
                        'link' => 'campaign.create'
                    ],
                ]
            ],
        ]
    ],
    'music' => [
        'name' => 'music',
        'title' => 'رادیو اسنپ',
        'icon' => 'fad fa-album-collection',
        'link' => 'music',
        'body' => []
    ],
    'reports' => [
        'name' => 'reports',
        'title' => 'گزارش‌گیری',
        'icon' => 'fad fa-analytics',
        'link' => 'reports',
        'body' => []
    ],
];
