@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست کمپین‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست کمپین‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($campaigns->total() > $campaigns->perPage())
                    <div class="card-footer">{{$campaigns->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($campaigns))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام</th>
                                <th scope="col">تاریخ شروع</th>
                                <th scope="col">تاریخ پایان</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($campaigns as $campaign)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($campaigns->currentPage()-1) * $campaigns->perPage())}}</th>
                                    <td><a href="{{route('category.show',$campaign->id)}}"> {{$campaign->name}}</a></td>
                                    <td>{{jdate($campaign->start)->format('y/m/d - H:m:s')}}</td>
                                    <td>{{jdate($campaign->end)->format('y/m/d - H:m:s')}}</td>
                                    <td>{{jdate($campaign->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($campaign->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('campaign.destroy',$campaign->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('campaign.edit',$campaign->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($campaigns->total() > $campaigns->perPage())
                    <div class="card-footer">{{$campaigns->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/category/activate/'+id , success: function(result){
                    toastr.info('وضعیت دسته‌بندی تغییر کرد!');

                }});
        }
    </script>
@endsection
