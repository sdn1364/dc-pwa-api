@extends('master')

@section('breadcrumb')
    <h3 class="page-title">دسته‌بندی جدید</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">لیست دسته‌بندی‌ها</a></li>
            <li class="breadcrumb-item active" aria-current="page">دسته‌بندی جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('campaign.store')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="">نام کمپین</label>
                            <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <select multiple="multiple" size="10" name="selectedProducts[]" class="form-control">
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">تاریخ شروع کمپین</label>
                                    <input type="text" name="startDate" class="date form-control text-left" dir="ltr" autocomplete="off">
                                </div>
                                <div class="col col-md-2">
                                    <label for="">زمان </label>
                                    <div class="input-group clockpicker-autoclose-demo">
                                        <div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fal fa-clock"></i>
									</span>
                                        </div>
                                        <input type="text" class="form-control text-left" name="startTime" dir="ltr" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for=""> تاریخ پایان کمپین</label>
                                    <input type="text" name="endDate" class=" date form-control text-left" dir="ltr" autocomplete="off">
                                </div>
                                <div class="col col-md-2">
                                    <label for="">زمان </label>
                                    <div class="input-group clockpicker-autoclose-demo">
                                        <div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fal fa-clock"></i>
									</span>
                                        </div>
                                        <input type="text" class="form-control text-left" name="endTime" dir="ltr" autocomplete="off"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">ذخیره</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('vendors/dual-list/bootstrap-duallistbox.min.css')}}">
    <!-- Clockpicker -->
    <link rel="stylesheet" href="{{asset('vendors/clockpicker/bootstrap-clockpicker.min.css')}}" type="text/css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="{{asset('vendors/datepicker-jalali/bootstrap-datepicker.min.css')}}">
@endsection
@section('scripts')
    <script>
        //  Form Validation
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
    <script src="{{asset('vendors/dual-list/jquery.bootstrap-duallistbox.js')}}"></script>
    <script>
        var demo1 = $('select[name="selectedProducts[]"]').bootstrapDualListbox();
    </script>
    <script src="{{asset('vendors/datepicker-jalali/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('vendors/datepicker-jalali/bootstrap-datepicker.fa.min.js')}}"></script>
    <script src="{{asset('assets/js/examples/datepicker.js')}}"></script>
    <script src="{{asset('vendors/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
    <script src="{{asset('assets/js/examples/clockpicker.js')}}"></script>
@endsection

