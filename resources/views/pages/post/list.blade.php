@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست پست‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست پست‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($posts->total() > $posts->perPage())
                    <div class="card-footer">{{$posts->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($posts))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">عنوان</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($posts->currentPage()-1) * $posts->perPage())}}</th>
                                    <td><a href="{{route('city.show',$post->id)}}"> {{$post->title}}</a></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch-{{$post->id}}" {{$post->status?'checked':null}} onChange="activate({{$post->id}})">
                                                <label class="custom-control-label" for="customSwitch-{{$post->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($post->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($post->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('city.destroy',$post->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('city.edit',$post->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($posts->total() > $posts->perPage())
                    <div class="card-footer">{{$posts->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/notification/activate/'+id , success: function(result){
                    toastr.info('وضعیت شهر تغییر کرد!');

                }});
        }
    </script>
@endsection
