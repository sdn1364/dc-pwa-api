@extends('master')

@section('breadcrumb')
    <h3 class="page-title">دسته‌بندی جدید</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">لیست دسته‌بندی‌ها</a></li>
            <li class="breadcrumb-item active" aria-current="page">دسته‌بندی جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('supplier.store')}}" method="post" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="">نام تامین‌کننده</label>
                            <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                            <div class="valid-feedback">
                                صحیح است!
                            </div>
                            <div class="invalid-feedback">
                                لطفا نام تامین‌کننده را وارد کنید.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md6">
                                <div class="form-group">
                                    <label for="">دسته‌بندی</label>
                                    <select class="custom-select custom-select-sm" name="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col col-md-6">
                                <div class="form-group">
                                    <label for="">شهر</label>
                                    <select class="custom-select custom-select-sm" name="city_id">
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">وضعیت تامین‌کننده</label>
                            <div class="custom-control custom-checkbox custom-checkbox-success">
                                <input type="hidden" name="status"  value="0">
                                <input type="checkbox" class="custom-control-input" name="status" id="customCheck2" value="1">
                                <label class="custom-control-label" for="customCheck2">فعال</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="ذخیره"/>
                            <input type="submit" name="submitNew" class="btn btn-secondary" value="ذخیره و جدید"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //  Form Validation
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection

