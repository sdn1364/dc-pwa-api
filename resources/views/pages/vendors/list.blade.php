@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست تامین‌کننده‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست تامین‌کننده‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($vendors->total() > $vendors->perPage())
                    <div class="card-header">{{$vendors->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($vendors))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام تامین‌کننده</th>
                                <th scope="col">دسته‌بندی</th>
                                <th scope="col">شهر</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendors as $vendor)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($vendors->currentPage()-1) * $vendors->perPage())}}</th>
                                    <td><a href="{{route('supplier.show',$vendor->id)}}"> {{$vendor->name}}</a></td>
                                    <td>{{$vendor->category->name}}</td>
                                    <td>{{$vendor->city->city}}</td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch-{{$vendor->id}}" {{$vendor->status?'checked':null}} onChange="activate({{$vendor->id}})">
                                                <label class="custom-control-label" for="customSwitch-{{$vendor->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($vendor->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($vendor->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('supplier.destroy',$vendor->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('supplier.edit',$vendor->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($vendors->total() > $vendors->perPage())
                    <div class="card-footer">{{$vendors->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/supplier/activate/'+id , success: function(result){
                    toastr.info('وضعیت تامین‌کننده تغییر کرد!');

                }});
        }
    </script>
@endsection
