@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست شهرها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست شهرها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($cities->total() > $cities->perPage())
                    <div class="card-footer">{{$cities->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($cities))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>

                                <th scope="col">شهر</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cities as $city)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($cities->currentPage()-1) * $cities->perPage())}}</th>
                                    <td><a href="{{route('city.show',$city->id)}}"> {{$city->city}}</a></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch-{{$city->id}}" {{$city->status?'checked':null}} onChange="activate({{$city->id}})">
                                                <label class="custom-control-label" for="customSwitch-{{$city->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($city->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($city->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('city.destroy',$city->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('city.edit',$city->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($cities->total() > $cities->perPage())
                    <div class="card-footer">{{$cities->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/city/activate/'+id , success: function(result){
                    toastr.info('وضعیت شهر تغییر کرد!');

                }});
        }
    </script>
@endsection
