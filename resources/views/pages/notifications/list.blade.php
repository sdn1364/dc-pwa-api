@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست پیام‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست پیام‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($notifs->total() > $notifs->perPage())
                    <div class="card-footer">{{$notifs->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($notifs))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">عنوان</th>
                                <th scope="col">پیام</th>
                                <th scope="col">نوع</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notifs as $notif)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($notifs->currentPage()-1) * $notifs->perPage())}}</th>
                                    <td><a href="{{route('city.show',$notif->id)}}"> {{$notif->name}}</a></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch-{{$notif->id}}" {{$notif->status?'checked':null}} onChange="activate({{$notif->id}})">
                                                <label class="custom-control-label" for="customSwitch-{{$notif->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($notif->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($notif->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('city.destroy',$notif->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('city.edit',$notif->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($notifs->total() > $notifs->perPage())
                    <div class="card-footer">{{$notifs->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/notification/activate/'+id , success: function(result){
                    toastr.info('وضعیت شهر تغییر کرد!');

                }});
        }
    </script>
@endsection
