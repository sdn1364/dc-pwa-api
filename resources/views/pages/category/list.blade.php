@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست دسته‌بندی‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست دسته‌بندی‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($categories->total() > $categories->perPage())
                    <div class="card-footer">{{$categories->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($categories))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام دسته‌بندی</th>
                                <th scope="col">عکس</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($categories->currentPage()-1) * $categories->perPage())}}</th>
                                    <td>{{$category->name}}</a></td>
                                    <td>@if($category->photo)  <i class="fal fa-check text-success"></i>@else<i class="fal fa-times text-danger"></i>@endif</td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch-{{$category->id}}" {{$category->status?'checked':null}} onChange="activate({{$category->id}})">
                                                <label class="custom-control-label" for="customSwitch-{{$category->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($category->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($category->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('category.destroy',$category->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <div class="btn-group btn-group-sm">
                                                    <a href="{{route('category.edit',$category->id)}}" class="btn btn-sm btn-outline-light">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <button type="submit" class="btn btn-sm btn-outline-light">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($categories->total() > $categories->perPage())
                    <div class="card-footer">{{$categories->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/category/activate/'+id , success: function(result){
                    toastr.info('وضعیت دسته‌بندی تغییر کرد!');

                }});
        }
    </script>
@endsection
