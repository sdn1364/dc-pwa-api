@extends('master')

@section('breadcrumb')
    <h3 class="page-title">جستحوی دسته‌بندی‌ها دسته‌بندی‌ها</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">جستجوی دسته‌بندی‌ها</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <form action="">
                        <div class="form-group">
                            <label for="">جستجو</label>
                            <input type="text" name=" " id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">وضعیت: </label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customCheckBoxInline1" name="status" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="customCheckBoxInline1">فعال</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customCheckBoxInline2" name="status" class="custom-control-input" value="0">
                                <label class="custom-control-label" for="customCheckBoxInline2">غیر فعال</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">دسته‌بندی والد</label>
                            <select class="form-control" name="" id="">
                                @forelse($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @empty
                                    <option>دسته‌بندی وجود ندارد</option>

                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">جستجو</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="alert alert-secondary text-center" role="alert">
                        نتیجه‌ای برای نمایش وجود ندارد.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
