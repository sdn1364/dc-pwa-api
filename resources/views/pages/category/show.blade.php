@extends('master')

@section('breadcrumb')
    <h3 class="page-title">{{$category->name}}</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$category->name}}</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h3><strong>نام دسته‌بندی:</strong> {{$category->name}}</h3>
                            <h4><strong>دسته‌بندی والد:</strong> {{$category->parent?$category->parent->name:'دسته والد'}}</h4>
                            <h4><strong>وضعیت دسته‌بندی:</strong> {{$category->status===1? 'فعال':'غیر‌فعال'}}</h4>
                        </div>‍
                        @if($category->photo != null)
                            <div class="col-md-4">
                                <img class="img-fluid img-thumbnail" src="{{asset('storage/'.$category->photo)}}" alt="">

                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
