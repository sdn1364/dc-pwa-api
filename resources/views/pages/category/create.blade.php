@extends('master')

@section('breadcrumb')
    <h3 class="page-title">دسته‌بندی جدید</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">لیست دسته‌بندی‌ها</a></li>
            <li class="breadcrumb-item active" aria-current="page">دسته‌بندی جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('category.store')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="">نام دسته‌بندی</label>
                            <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                            <div class="valid-feedback">
                                صحیح است!
                            </div>
                            <div class="invalid-feedback">
                                لطفا نام دسته‌بندی را وارد کنید.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">عکس</label>
                            <div class="custom-file">
                                <input type="file" name="photo" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">وضعیت دسته‌بندی</label>
                            <div class="custom-control custom-checkbox custom-checkbox-success">
                                <input type="hidden" name="status"  value="0">
                                <input type="checkbox" class="custom-control-input" name="status" id="customCheck2" value="1">
                                <label class="custom-control-label" for="customCheck2">فعال</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="ذخیره"/>
                            <input type="submit" name="submitNew" class="btn btn-secondary" value="ذخیره و جدید"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-transparent">
                <div class="card-header">
                    <h5>راهنما</h5>
                </div>
                <div class="card-body">
                    <p><small>
                            در این صفحه می‌توانید دسته‌بندی‌های جدید بسازید.
                        </small></p>
                    <p class="m-b-0">نام دسته‌بندی</p>
                    <p><small>توجه داشته باشید که نام دسته‌بندی را حتما وارد کنید.</small></p>
                    <p class="m-b-0">دسته‌بندی والد</p>
                    <p class="text-justify"><small class="text-justify">در صورتی که می‌خواهید دسته‌بندی مورد نظرتان زیرگروه دسته‌بندی دیگری قرار گیرد، نام دسته‌بندی مورد نظر را از این لیست انتخاب کنید. در غیر این صورت این گزینه را روی <strong>«والد»</strong> تنظیم کنید.</small></p>
                    <p class="m-b-0">عکس دسته‌بندی</p>
                    <p class="text-justify"><small class="text-justify">هر دسته‌بندی می‌تواند یک عکس داشته باشد. این عکس به عنوان یک آیکون در اپلیکیشن نمایش داده می‌شود. توجه داشته باشید که آپلود عکس اجباری نیست اما دسته‌بندی‌هایی که عکس نداشته باشند در صفحه اصلی اپلیکیشن نمایش داده نمی‌شوند.</small></p>
                    <p class="m-b-0">وضعیت دسته‌بندی</p>
                    <p class="text-justify"><small class="text-justify">به صورت پیش‌فرض دسته‌بندی، دسته‌بندی‌هایی که فعال نباشند در هیچ لیستی نمایش داده نمی‌شود.</small></p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //  Form Validation
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection

