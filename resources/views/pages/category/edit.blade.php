@extends('master')

@section('breadcrumb')
    <h3 class="page-title">{{$category->name}}</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">لیست دسته‌بندی‌ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('category.index')}}">ویرایش سته‌بندی‌ها</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$category->name}}</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('category.store')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="">نام دسته‌بندی</label>
                            <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId" required="" value="{{$category->name}}">
                            <div class="valid-feedback">
                                صحیح است!
                            </div>
                            <div class="invalid-feedback">
                                لطفا نام دسته‌بندی را وارد کنید.
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">عکس</label>
                            <div class="custom-file">
                                <input type="file" name="photo" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">وضعیت دسته‌بندی</label>
                            <div class="custom-control custom-checkbox custom-checkbox-success">
                                <input type="hidden" name="status" value="0">
                                <input type="checkbox" class="custom-control-input" name="status" id="customCheck2" value="1" {{$category->status == 1 ? 'checked':null}}>
                                <label class="custom-control-label" for="customCheck2">فعال</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">ذخیره</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if($category->photo)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <img class="img-thumbnail img-fluid" src="{{asset('storage/'.$category->photo)}}" alt="">
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@section('scripts')
    <script>
        //  Form Validation
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection

