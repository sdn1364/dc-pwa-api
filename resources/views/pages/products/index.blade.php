@extends('master')

@section('breadcrumb')
    <h3 class="page-title">لیست محصولات</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">لیست محصولات</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                @if($products->total() > $products->perPage())
                    <div class="card-footer">{{$products->links()}}</div>
                @endif
                <div class="card-body">
                    @if(count($products))
                        <table class="table" id="dataTable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام محصول</th>
                                <th scope="col">دسته‌بندی</th>
                                <th scope="col">تامین کننده</th>
                                <th scope="col">قیمت</th>
                                <th scope="col">تخفیف</th>
                                <th scope="col">سقف رزرو</th>
                                <th scope="col">تعداد</th>
                                <th scope="col">عکس</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">ویژه</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">تاریخ ویرایش</th>
                                <th class="text-right" scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <th scope="row">{{$loop->iteration + (($products->currentPage()-1) * $products->perPage())}}</th>
                                    <td><a href="{{route('product.show',$product->id)}}"> {{$product->title}}</a></td>
                                    <td>{{$product->category->name}}</td>
                                    <td>{{$product->vendor->name}}</td>
                                    <td>{{$product->price}} <small>تومان</small></td>
                                    <td>{{$product->discount}}%</td>
                                    <td>{{$product->max_reserved}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td>@if($product->photo)  <i class="fal fa-check text-success"></i>@else<i class="fal fa-times text-danger"></i>@endif</td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="statusSwitch-{{$product->id}}" {{$product->status?'checked':null}} onChange="activate({{$product->id}})">
                                                <label class="custom-control-label" for="statusSwitch-{{$product->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-checkbox-success">
                                                <input type="checkbox" class="custom-control-input" id="featureSwitch-{{$product->id}}" {{$product->is_featured?'checked':null}} onChange="feature({{$product->id}})">
                                                <label class="custom-control-label" for="featureSwitch-{{$product->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{jdate($product->created_at)->format('y/m/d')}}</td>
                                    <td>{{jdate($product->updated_at)->format('y/m/d')}}</td>
                                    <td class="text-right">
                                        <div class="col-md-12">
                                            <form action="{{route('product.destroy',$product->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="{{route('product.edit',$product->id)}}" class="btn btn-sm  btn-outline-secondary btn-square">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm btn-outline-secondary btn-square">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-secondary text-center" role="alert">
                            رکوردی برای نمایش وجود ندارد.
                        </div>
                    @endif
                </div>

                @if($products->total() > $products->perPage())
                    <div class="card-footer">{{$products->links()}}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>

        function activate(id){
            $.ajax({url: '/api/v1/product/activate/'+id , success: function(result){
                    toastr.info('وضعیت محصول تغییر کرد!');

                }});
        }
        function feature(id){
            $.ajax({url: '/api/v1/product/feature/'+id , success: function(result){
                    toastr.info('وضعیت محصول تغییر کرد!');

                }});
        }
    </script>
@endsection
