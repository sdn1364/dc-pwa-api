@extends('master')

@section('breadcrumb')
    <h3 class="page-title">محصول جدید</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item"><a href="{{route('product.index')}}">لیست محصولات</a></li>
            <li class="breadcrumb-item active" aria-current="page">محصول جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="">نام محصول</label>
                            <input type="text" name="title" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                            <div class="valid-feedback">
                                صحیح است!
                            </div>
                            <div class="invalid-feedback">
                                لطفا نام محصول را وارد کنید.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md4">
                                <div class="form-group">
                                    <label for="">دسته‌بندی</label>
                                    <select class="custom-select custom-select-sm" name="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col col-md-4">

                                <div class="form-group">
                                    <label for="">تامین‌کننده</label>
                                    <select class="custom-select custom-select-sm" name="vendor_id">
                                        @foreach($vendors as $vendor)
                                            <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col col-md-4">
                                <div class="form-group">
                                    <label for="">شهر</label>
                                    <select class="custom-select custom-select-sm" name="city_id">
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-md-4">
                                <div class="form-group">
                                    <label for="">قیمت</label>
                                    <input type="text" data-input-mask="money" name="price" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                                </div>
                            </div>
                            <div class="col col-md-4">
                                <div class="form-group">
                                    <label for="">تخفیف</label>
                                    <input type="text" name="discount" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                                </div>
                            </div>
                            <div class="col col-md-4">
                                <div class="row">
                                    <div class="col col-md-6">
                                        <div class="form-group">
                                            <label for="">تعداد</label>
                                            <input type="number" name="quantity" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                                        </div>
                                    </div>
                                    <div class="col col-md-6">
                                        <div class="form-group">
                                            <label for="">تعداد رزرو</label>
                                            <input type="number" name="max_reserved" id="" class="form-control" placeholder="" aria-describedby="helpId" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">عکس</label>
                            <div class="custom-file">
                                <input type="file" name="photo" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">توضیحات</label>
                            <textarea class="form-control" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">وضعیت محصول</label>
                            <div class="custom-control custom-checkbox custom-checkbox-success">
                                <input type="hidden" name="status"  value="0">
                                <input type="checkbox" class="custom-control-input" name="status" id="customCheck2" value="1">
                                <label class="custom-control-label" for="customCheck2">فعال</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="ذخیره"/>
                            <input type="submit" name="submitNew" class="btn btn-secondary" value="ذخیره و جدید"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //  Form Validation
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
    <script src="{{asset('vendors/input-mask/jquery.mask.js')}}"></script>
    <script src="{{asset('assets/js/examples/input-mask.js')}}"></script>
@endsection

