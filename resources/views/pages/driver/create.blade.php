@extends('master')

@section('breadcrumb')
    <h3 class="page-title">بروزرسانی اطلاعات کاربران راننده</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">داشبورد</a></li>
            <li class="breadcrumb-item active" aria-current="page">بروزرسانی اطلاعات کاربران راننده</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row ">
                        <div class="col col-md-6 ">
                            <h5>تعداد کاربران راننده فعال: {{$driverCount}} نفر </h5>

                        </div>
                        <div class="col col-md-6 flex-row justify-content-end">
                            <form action="{{route('driver.clear')}}" method="post">
                                @csrf
                                <div class="form-group  ">
                                    <button type="submit" class="btn btn-danger btn-sm {{$driverCount == 0 ? 'disabled' : null}}" {{$driverCount == 0 ? 'disabled' : null}}>پاک کردن اطلاعات قدیمی کاربران راننده</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">

                    <form action="{{route('driver.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">فایل CSV</label>
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">ذخیره</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('styles')
    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('vendors/dropzone/dropzone.css')}}" type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('vendors/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/js/examples/dropzone.js')}}"></script>
@endsection


