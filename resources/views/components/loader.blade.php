<!-- begin::page loader-->
<div class="page-loader">
    <img src="{{asset('assets/media/svg/logo-small.svg')}}" height="50" alt="image">
    <br>
    <div class="spinner-border"></div>
</div>
<!-- end::page loader -->
