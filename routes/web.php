<?php
Route::middleware('auth')->group( function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/sellsDashboard', 'DashController@sell')->name('sellsDashboard');

    Route::resources([
        'category'     =>  'CategoriesController',
        'product'      =>  'ProductsController',
        'city'         =>  'CitiesController',
        'supplier'     =>  'VendorsController',
        'visitor'      =>  'VisitorsController',
        'log'          =>  'LogsController',
        'campaign'     =>  'CampaignsController',
        'user'         =>  'usersController',
        'permission'   =>  'PermissionsController',
        'driver'       =>  'DriversController',
        'notification' =>  'NotificationsController',
        'blog'         =>  'PostsController'
    ]);


    Route::post('driver/clear', 'DriversController@clear')->name('driver.clear');
});
Auth::routes();


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
