<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::prefix('v1')->namespace('Api\v1')->group(function () {
    Route::post('check-driver', 'DriversController@checkDriver');
    Route::prefix('category')->group(function() {
        Route::get('/', 'CategoriesController@index');
        Route::get('/activate/{category}', 'CategoriesController@activate');
    });
    Route::prefix('product')->group(function(){
        Route::get('/getFeatured', 'ProductsController@getFeatured');
        Route::get('/getLatest', 'ProductsController@getLatest');
        Route::get('/{product}', 'ProductsController@getOne');
        Route::get('/activate/{product}', 'ProductsController@activate');
        Route::get('/feature/{product}', 'ProductsController@feature');
        Route::get('/decrement/{product}', 'ProductsController@decrementingProduct');
        Route::get('/increment/{product}', 'ProductsController@incrementingProduct');
    });
    Route::prefix('city')->group(function() {
        Route::get('/', 'CitiesController@all');
        Route::get('/activate/{city}', 'CitiesController@activate');
    });
    Route::prefix('supplier')->group(function() {
        Route::get('/', 'VendorsController@index');
        Route::get('/activate/{vendor}', 'VendorsController@activate');
    });
    Route::prefix('campaign')->group(function(){
        Route::get('/latestCampaign', 'CampaignsController@getLatestCampaign');
    });
    Route::prefix('post')->group(function(){
        Route::get('/latestPosts','PostsController@latest');
    });
    Route::prefix('purchased','PurchasesController@add');
});
