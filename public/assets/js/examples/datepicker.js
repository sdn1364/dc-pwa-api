'use strict';
$(document).ready(function () {

	// ------

	$('.date').datepicker({
        dateFormat: "yy/mm/dd",
        showOtherMonths: true,
        selectOtherMonths: true,
        minDate: 0,
        maxDate: "+14D"
	});
});
