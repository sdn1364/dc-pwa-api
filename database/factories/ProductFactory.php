<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => \Ybazli\Faker\Facades\Faker::word().' '.\Ybazli\Faker\Facades\Faker::word().' '.\Ybazli\Faker\Facades\Faker::word(),
        'description' => \Ybazli\Faker\Facades\Faker::paragraph(),
        'photo' => 'http://placeimg.com/1000/500/'.$faker->randomElement($array = array ('animals','nature','arch','people','tech')),
        'discount' => $faker->numberBetween(0,50),
        'quantity' => $faker->numberBetween(1,10),
        'price' => $faker->randomNumber(),
        'city_id' => factory(\App\City::class),
        'vendor_id' => factory(\App\Vendor::class),
        'category_id' => factory(\App\Category::class),
        'max_reserved' => $faker->numberBetween(1,10),
        'published_at' => $faker->dateTime(),
    ];
});
