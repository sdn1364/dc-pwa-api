<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Visitor;
use Faker\Generator as Faker;

$factory->define(Visitor::class, function (Faker $faker) {
    return [
        'ip' => $faker->word,
        'useragent' => $faker->word,
        'phone' => $faker->phoneNumber,
        'is_driver' => $faker->boolean,
        'city' => $faker->city,
    ];
});
