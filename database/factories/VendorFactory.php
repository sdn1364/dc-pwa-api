<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => \Ybazli\Faker\Facades\Faker::word(),
        'category_id' => factory(\App\Category::class),
        'city_id' => factory(\App\City::class),
    ];
});
